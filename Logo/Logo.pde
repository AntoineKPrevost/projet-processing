int rayon = 250;
float rayonInter = rayon*cos(PI/6);
float rayonCentral = rayon*cos(PI/3);
float rayonCentral2 = rayon*cos(PI/3) - 25;
float rayonDeco = (rayonInter - rayonCentral)/2;

float listPoint[] = {
  PI/6,
  3*PI/6,
  5*PI/6,
  7*PI/6,
  9*PI/6,
  11*PI/6,
};

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  translate(width/2, height/2);
  stroke(255);
  noFill();
  //double cercle externe
  circle(0, 0, 2*rayon);
  circle(0, 0, 2*rayon +50);
  //cercle interne
  circle(0, 0, 2*rayonInter);
  //double cercle central
  circle(0, 0, 2*rayonCentral2);
  circle(0, 0, 2*rayonCentral);
  //cercle décoratif
  circle((rayonDeco + rayonCentral)*cos(listPoint[0]), (rayonDeco + rayonCentral)*sin(listPoint[0]), 2*rayonDeco);
  circle((rayonDeco + rayonCentral)*cos(listPoint[2]), (rayonDeco + rayonCentral)*sin(listPoint[2]), 2*rayonDeco);
  circle((rayonDeco + rayonCentral)*cos(listPoint[4]), (rayonDeco + rayonCentral)*sin(listPoint[4]), 2*rayonDeco);
  //Hexagone
  line(rayon*cos(listPoint[0]), rayon*sin(listPoint[0]), rayon*cos(listPoint[1]), rayon*sin(listPoint[1]));
  line(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), rayon*cos(listPoint[2]), rayon*sin(listPoint[2]));
  line(rayon*cos(listPoint[2]), rayon*sin(listPoint[2]), rayon*cos(listPoint[3]), rayon*sin(listPoint[3]));
  line(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), rayon*cos(listPoint[4]), rayon*sin(listPoint[4]));
  line(rayon*cos(listPoint[4]), rayon*sin(listPoint[4]), rayon*cos(listPoint[5]), rayon*sin(listPoint[5]));
  line(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), rayon*cos(listPoint[0]), rayon*sin(listPoint[0]));
  //tracé interne
  line(rayon*cos(listPoint[0]), rayon*sin(listPoint[0]), rayonInter*cos(listPoint[0]), rayonInter*sin(listPoint[0]));
  line(rayonCentral*cos(listPoint[0]), rayonCentral*sin(listPoint[0]), rayonCentral2*cos(listPoint[0]), rayonCentral2*sin(listPoint[0]));
  line(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), rayonCentral*cos(listPoint[1]), rayonCentral*sin(listPoint[1]));
  line(rayon*cos(listPoint[2]), rayon*sin(listPoint[2]), rayonInter*cos(listPoint[2]), rayonInter*sin(listPoint[2]));
  line(rayonCentral*cos(listPoint[2]), rayonCentral*sin(listPoint[2]), rayonCentral2*cos(listPoint[2]), rayonCentral2*sin(listPoint[2]));
  line(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), rayonCentral*cos(listPoint[3]), rayonCentral*sin(listPoint[3]));
  line(rayon*cos(listPoint[4]), rayon*sin(listPoint[4]), rayonInter*cos(listPoint[4]), rayonInter*sin(listPoint[4]));
  line(rayonCentral*cos(listPoint[4]), rayonCentral*sin(listPoint[4]), rayonCentral2*cos(listPoint[4]), rayonCentral2*sin(listPoint[4]));
  line(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), rayonCentral*cos(listPoint[5]), rayonCentral*sin(listPoint[5]));
  //Triangle
  strokeWeight(3);
  line(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), rayon*cos(listPoint[3]), rayon*sin(listPoint[3]));
  line(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), rayon*cos(listPoint[3]), rayon*sin(listPoint[3]));
  line(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), rayon*cos(listPoint[5]), rayon*sin(listPoint[5]));
  line(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), rayon*cos(listPoint[5]), rayon*sin(listPoint[5]));
  line(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), rayon*cos(listPoint[1]), rayon*sin(listPoint[1]));
  line(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), rayon*cos(listPoint[1]), rayon*sin(listPoint[1]));
  strokeWeight(1);
  //Arc de cercle
  arc(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), 2*rayonCentral2 + 25, 2*rayonCentral2 + 25, 7*PI/6, 11*PI/6, OPEN);
  arc(rayon*cos(listPoint[1]), rayon*sin(listPoint[1]), 2*rayonCentral2 - 25, 2*rayonCentral2 - 25, 7*PI/6, 11*PI/6, OPEN);
  arc(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), 2*rayonCentral2 + 25, 2*rayonCentral2 + 25, 11*PI/6, 2*PI, OPEN);
  arc(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), 2*rayonCentral2 + 25, 2*rayonCentral2 + 25, 0, 3*PI/6, OPEN);
  arc(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), 2*rayonCentral2 - 25, 2*rayonCentral2 - 25, 11*PI/6, 2*PI, OPEN);
  arc(rayon*cos(listPoint[3]), rayon*sin(listPoint[3]), 2*rayonCentral2 - 25, 2*rayonCentral2 - 25, 0, 3*PI/6, OPEN);
  arc(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), 2*rayonCentral2 + 25, 2*rayonCentral2 + 25, 3*PI/6, 7*PI/6, OPEN);
  arc(rayon*cos(listPoint[5]), rayon*sin(listPoint[5]), 2*rayonCentral2 - 25, 2*rayonCentral2 - 25, 3*PI/6, 7*PI/6, OPEN);
  
}
