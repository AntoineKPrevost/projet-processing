class Cell implements Comparable {
  
  int x, y;
  boolean[] walls = {true, true, true, true};
  boolean visited;
  float fScore = -1;
  float gScore = -1;
  float hScore = -1;
  Cell origine;

  
  Cell(int i, int j){
    x = i;
    y = j;
    visited = false;
  }

  void setVisited(){
    visited = true;
  }
  
  boolean getVisited(){
    return visited;
  }
  
  void remove(int index){
    walls[index] = false;
  }
  
  void draw(){
    if(visited){
      noStroke();
      fill(100, 0, 255, 25);
      rect(x*w, y*w, w, w);
    }
    stroke(0);
    noFill();
    if(walls[0]){
      line(x * w, y * w, (x + 1) * w, y * w);
    }
    if(walls[1]){
      line((x + 1) * w, y * w, (x + 1) * w, (y + 1) * w);
    }
    if(walls[2]){
      line((x + 1) * w, (y + 1) * w, x * w, (y + 1) * w);
    }
    if(walls[3]){
      line(x * w, (y + 1) * w, x * w, y * w);
    }
  }
  
  ArrayList<Cell> getNeighbors() {
    ArrayList neighbors = new ArrayList();
    if(!walls[0] && y > 0){
      neighbors.add(grid[x][y - 1]);
    }
    if(!walls[1] && x < cols - 1){
      neighbors.add(grid[x + 1][y]);
    }
    if(!walls[2] && y < rows - 1){
      neighbors.add(grid[x][y + 1]);
    }
    if(!walls[3] && x > 0){
      neighbors.add(grid[x - 1][y]);
    }
    return neighbors;
  }
  
  int compareTo(Object o) {
    Cell autre = (Cell)o;
    if (fScore > autre.fScore) {
      return 1;
    }
    if (fScore < autre.fScore) {
      return -1;
    }
    return 0;
  }
}
