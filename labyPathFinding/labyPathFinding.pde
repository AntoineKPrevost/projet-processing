int etat;
final int INIT = 0;
final int GENE = 1;
final int PATH = 2;

int cols, rows;
int w = 10;
color colorPath;

Cell[][] grid;
Cell[] stack = {};
Cell currentLab, next, depart, objectif;

ArrayList<Cell> openList = new ArrayList();
ArrayList<Cell> closeList = new ArrayList();

void setup(){
  size(600, 600);
  etat = INIT;
  cols = width / w;
  rows = height / w;
  
  //setup création labyrinthe
  initLaby();
  initPath();
}

void draw(){
  background(255);
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j].draw();
      if(grid[i][j] == currentLab){
        noStroke();
        fill(255, 0, 100, 100);
        rect(i*w, j*w, w, w);
      }
    }
  }
  
  switch(etat){
    case INIT :
      break;
    case GENE :
      if(stack.length != 0){
        Cell[] voisins = getVoisinGen(currentLab);
        if(voisins.length != 0){
          int ind = int(random(voisins.length));
          next = voisins[ind];
          removeWall(currentLab, next);
          stack = (Cell[])append(stack, next);
          grid[next.x][next.y].setVisited();
          currentLab = next;
        }else{
          currentLab = grid[stack[stack.length -1].x][stack[stack.length -1].y];
          stack = (Cell[])shorten(stack);
        }
      }
      break;
    case PATH :
    //frameRate(5);
      if (openList.size() > 0) {
      java.util.Collections.sort(openList);
      Cell current = openList.remove(0);
      closeList.add(current);
      colorPath = color(255, 0, 255);
      if (current == objectif) {
        noLoop();
        colorPath = color(0, 0, 255);
        print("fini");
      }
      float tempCout = 0;
      ArrayList<Cell> neighbors = current.getNeighbors();
      for (Cell c : neighbors) {
        tempCout = current.gScore + distance(c, current);
        if (closeList.indexOf(c) == -1 && c.fScore < tempCout) {
          if (openList.indexOf(c) == -1) {
            openList.add(c);
          }
          if (c.gScore < tempCout) {
              c.origine = current;
              c.hScore = distance(c, objectif);
              c.fScore = c.gScore + c.hScore;
            }
        }
        
      ArrayList<Cell> path = getPath(current);
      for (Cell cell : path) {
        stroke(colorPath);
        line((cell.x + 0.5) * w, (cell.y + 0.5) * w, (cell.origine.x + 0.5) * w, (cell.origine.y + 0.5) * w);
      }
      }
    }
      break;
  }
}

void keyPressed(){
  if(key == ' '){
    etat ++;
    if(etat > PATH){
      etat = INIT;
    }
  }
}

void initLaby(){
  grid = new Cell[cols][rows];
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j] = new Cell(i, j);
    }
  }
  currentLab = grid[int(random(cols))][int(random(rows))];
  stack = (Cell[])append(stack, currentLab);
  currentLab.setVisited();
   depart = currentLab;
}

void initPath(){
  depart.fScore = 0;
  depart.gScore = 0;
  depart.hScore = 0;
  objectif = grid[cols-1][0];
  openList.add(depart);
}

Cell[] getVoisinGen(Cell current){
  Cell[] voisin = {};
  if(current.x > 0 && !grid[current.x - 1][current.y].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x - 1][current.y]);
  }
  if(current.x < cols - 1 && !grid[current.x + 1][current.y].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x + 1][current.y]);
  }
  if(current.y > 0 && !grid[current.x][current.y - 1].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x][current.y - 1]);
  }
  if(current.y < rows - 1 && !grid[current.x][current.y + 1].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x][current.y + 1]);
  }
  return voisin;
}

void removeWall(Cell current, Cell next){
  switch(current.x - next.x){
    case -1 :
      grid[current.x][current.y].remove(1);
      grid[next.x][next.y].remove(3);
      break;
    case 1 :
      grid[current.x][current.y].remove(3);
      grid[next.x][next.y].remove(1);
      break;
  }
  switch(current.y - next.y){
    case -1 :
      grid[current.x][current.y].remove(2);
      grid[next.x][next.y].remove(0);
      break;
    case 1 :
      grid[current.x][current.y].remove(0);
      grid[next.x][next.y].remove(2);
      break;
  }
}

ArrayList<Cell> getPath(Cell c) {
  ArrayList<Cell> path = new ArrayList();
  while (c.origine != null) {
    path.add(c);
    c = c.origine;
  }
  return path;
}

float distance(Cell from, Cell goal) {
  return dist(from.x, from.y, goal.x, goal.y);
}
