class Rayon{
  
  PVector pos, dir;
  
  Rayon(PVector Ppos, float angle){
    pos = new PVector(Ppos.x, Ppos.y);
    dir = PVector.fromAngle(angle);
  }
  
  void updateMag(float dist){
    dir.setMag(dist);
  }
  
  void update(PVector Ppos, float angle){
    pos = Ppos;
    dir = PVector.fromAngle(angle);
  }
  
  PVector cast(Wall wall){
    float x1 = wall.debut.x;
    float y1 = wall.debut.y;
    float x2 = wall.fin.x;
    float y2 = wall.fin.y;
    
    float x3 = pos.x;
    float y3 = pos.y;
    float x4 = pos.x + dir.x;
    float y4 = pos.y + dir.y;
    
    float tNum = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4);
    float uNum = (x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3);
    float gDen = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    
    float t = tNum / gDen;
    float u = - uNum / gDen;
    
    if( t >= 0 && t <= 1 && u >= 0){
      return new PVector(x1 + t *(x2 - x1), y1 + t *(y2 - y1));
    }
    return null;
  }
  
  void draw(){
    float alpha = 75;
    stroke(255, alpha);
    line(0, 0, dir.x, dir.y);
  }
}
