import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Raycasting extends PApplet {

Wall[] walls;
Player player;

public void setup() {
  
  //fullScreen();
  walls = new Wall[10];
  player = new Player(width / 2, height / 2);

  for (int i = 0; i < walls.length - 4; i++) {
    walls[i] = new Wall(random(width), random(height), random(width), random(height));
  }
  walls[walls.length - 4] = new Wall(0, 0, width, 0);
  walls[walls.length - 3] = new Wall(width, 0, width, height);
  walls[walls.length - 2] = new Wall(width, height, 0, height);
  walls[walls.length - 1] = new Wall(0, height, 0, 0);
}

public void draw() {
  background(0);
  player.update(walls);
  for (Wall wall : walls) {
    wall.draw();
  }
  player.draw();
}

public void keyPressed() {
  player.setMove(keyCode, true);
}
 
public void keyReleased() {
  player.setMove(keyCode, false);
}
class Player{
  
    PVector pos, dir;
    Rayon[] rays;
    int isLeft, isRight, isUp, isDown;
  
  Player(float x, float y){
    pos = new PVector(x, y);
    dir = new PVector(1, 0);
    rays = new Rayon[181];
    for(int i = 0; i < rays.length; i++){
      rays[i] = new Rayon(pos, radians((i - (rays.length - 1) / 2)* 40 / (rays.length - 1 )));
    }
  }
  
  public void update(Wall[] walls){
    deplacement();
    dir.set(mouseX - pos.x, mouseY - pos.y);
    dir.normalize();
    for(int i = 0; i < rays.length; i++){
      float dist = sqrt(width * width + height * height);
      //float dist = 50;
      rays[i].update(pos, dir.heading() + radians((i - (rays.length - 1) / 2)* 40 / (rays.length - 1 )));
      for(Wall wall : walls){
        PVector pt = rays[i].cast(wall);
        if(pt != null && pos.dist(pt) < dist){
          dist = pos.dist(pt);
          rays[i].updateMag(dist);
        }
      }
    }
  }
  
  public void deplacement(){
    int vspd = (isRight - isLeft) * 4;
    int hspd = (isDown - isUp) * 4;
    if(pos.x + vspd <= 0 || pos.x + vspd >= width){
      while(!(pos.x + sign(vspd) <= 0 || pos.x + sign(vspd) >= width)){
        pos.x += sign(vspd);
      }
      vspd = 0;
    }
    if(pos.y + hspd <= 0 || pos.y + hspd >= height){
      while(!(pos.y + sign(hspd) <= 0 || pos.y + sign(hspd) >= height)){
        pos.y += sign(hspd);
      }
      hspd = 0;
    }
    pos.add(new PVector(vspd, hspd));
  }
  
  public void draw(){
    stroke(255);
    ellipse(pos.x, pos.y, 8, 8);
    translate(pos.x, pos.y);
    stroke(0, 255, 0);
    line(0, 0, dir.x * 30, dir.y * 30);
    for(Rayon ray : rays){
      ray.draw();
    }
  }
  
  
  public void setMove(int k, boolean b) {
    switch (k) {
    case UP:
      isUp = b ? 1 : 0;
      break;
   
    case DOWN:
      isDown = b ? 1 : 0;
      break;
   
    case LEFT:
      isLeft = b ? 1 : 0;
      break;
   
    case RIGHT:
      isRight = b ? 1 : 0;
      break;
   
    }
  }
}
class Rayon{
  
  PVector pos, dir;
  
  Rayon(PVector Ppos, float angle){
    pos = new PVector(Ppos.x, Ppos.y);
    dir = PVector.fromAngle(angle);
  }
  
  public void updateMag(float dist){
    dir.setMag(dist);
  }
  
  public void update(PVector Ppos, float angle){
    pos = Ppos;
    dir = PVector.fromAngle(angle);
  }
  
  public PVector cast(Wall wall){
    float x1 = wall.debut.x;
    float y1 = wall.debut.y;
    float x2 = wall.fin.x;
    float y2 = wall.fin.y;
    
    float x3 = pos.x;
    float y3 = pos.y;
    float x4 = pos.x + dir.x;
    float y4 = pos.y + dir.y;
    
    float tNum = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4);
    float uNum = (x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3);
    float gDen = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    
    float t = tNum / gDen;
    float u = - uNum / gDen;
    
    if( t >= 0 && t <= 1 && u >= 0){
      return new PVector(x1 + t *(x2 - x1), y1 + t *(y2 - y1));
    }
    return null;
  }
  
  public void draw(){
    float alpha = 75;
    stroke(255, alpha);
    line(0, 0, dir.x, dir.y);
  }
}
public int sign(float val){
  if(val == 0){
    return 0;
  }else if(val < 0){
    return -1;
  }else{
    return 1;
  }
}
class Wall{
  
  PVector debut, fin;
  
  Wall(float x1, float y1, float x2, float y2){
    debut = new PVector(x1, y1);
    fin = new PVector(x2, y2);
  }
  
  public void draw(){
    stroke(50, 50);
    line(debut.x, debut.y, fin.x, fin.y);
  }
}
  public void settings() {  size(800, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Raycasting" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
