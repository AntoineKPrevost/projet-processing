Wall[] walls;
Player player;

void setup() {
  size(800, 800);
  //fullScreen();
  walls = new Wall[10];
  player = new Player(width / 2, height / 2);

  for (int i = 0; i < walls.length - 4; i++) {
    walls[i] = new Wall(random(width), random(height), random(width), random(height));
  }
  walls[walls.length - 4] = new Wall(0, 0, width, 0);
  walls[walls.length - 3] = new Wall(width, 0, width, height);
  walls[walls.length - 2] = new Wall(width, height, 0, height);
  walls[walls.length - 1] = new Wall(0, height, 0, 0);
}

void draw() {
  background(0);
  player.update(walls);
  for (Wall wall : walls) {
    wall.draw();
  }
  player.draw();
}

void keyPressed() {
  player.setMove(keyCode, true);
}
 
void keyReleased() {
  player.setMove(keyCode, false);
}
