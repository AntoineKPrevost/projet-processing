class Player{
  
    PVector pos, dir;
    Rayon[] rays;
    int isLeft, isRight, isUp, isDown;
  
  Player(float x, float y){
    pos = new PVector(x, y);
    dir = new PVector(1, 0);
    rays = new Rayon[181];
    for(int i = 0; i < rays.length; i++){
      rays[i] = new Rayon(pos, radians((i - (rays.length - 1) / 2)* 40 / (rays.length - 1 )));
    }
  }
  
  void update(Wall[] walls){
    deplacement();
    dir.set(mouseX - pos.x, mouseY - pos.y);
    dir.normalize();
    for(int i = 0; i < rays.length; i++){
      float dist = sqrt(width * width + height * height);
      //float dist = 50;
      rays[i].update(pos, dir.heading() + radians((i - (rays.length - 1) / 2)* 40 / (rays.length - 1 )));
      for(Wall wall : walls){
        PVector pt = rays[i].cast(wall);
        if(pt != null && pos.dist(pt) < dist){
          dist = pos.dist(pt);
          rays[i].updateMag(dist);
        }
      }
    }
  }
  
  void deplacement(){
    int vspd = (isRight - isLeft) * 4;
    int hspd = (isDown - isUp) * 4;
    if(pos.x + vspd <= 0 || pos.x + vspd >= width){
      while(!(pos.x + sign(vspd) <= 0 || pos.x + sign(vspd) >= width)){
        pos.x += sign(vspd);
      }
      vspd = 0;
    }
    if(pos.y + hspd <= 0 || pos.y + hspd >= height){
      while(!(pos.y + sign(hspd) <= 0 || pos.y + sign(hspd) >= height)){
        pos.y += sign(hspd);
      }
      hspd = 0;
    }
    pos.add(new PVector(vspd, hspd));
  }
  
  void draw(){
    stroke(255);
    ellipse(pos.x, pos.y, 8, 8);
    translate(pos.x, pos.y);
    stroke(0, 255, 0);
    line(0, 0, dir.x * 30, dir.y * 30);
    for(Rayon ray : rays){
      ray.draw();
    }
  }
  
  
  void setMove(int k, boolean b) {
    switch (k) {
    case UP:
      isUp = b ? 1 : 0;
      break;
   
    case DOWN:
      isDown = b ? 1 : 0;
      break;
   
    case LEFT:
      isLeft = b ? 1 : 0;
      break;
   
    case RIGHT:
      isRight = b ? 1 : 0;
      break;
   
    }
  }
}
