class Wall{
  
  PVector debut, fin;
  
  Wall(float x1, float y1, float x2, float y2){
    debut = new PVector(x1, y1);
    fin = new PVector(x2, y2);
  }
  
  void draw(){
    stroke(255, 50);
    line(debut.x, debut.y, fin.x, fin.y);
  }
}
