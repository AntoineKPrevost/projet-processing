float angleBase, rayonBase;
float angle1, rayon1;
PVector[] vertices = {};

void setup() {
  size(600, 600);
  angleBase = 0;
  rayonBase = 100;
  angle1 = 0;
  rayon1 = 50;
}

void draw() {
  frameRate(15);
  background(0);
  strokeWeight(1);
  stroke(255);
  noFill();
  translate(width / 2, height / 2);
  circle(0, 0, rayonBase * 2);
  circle((rayonBase + rayon1) * cos(angleBase), (rayonBase + rayon1) * sin(angleBase), rayon1 * 2);
  line(0, 0, rayonBase * cos(angleBase), rayonBase * sin(angleBase));
  line((rayonBase + rayon1) * cos(angleBase), (rayonBase + rayon1) * sin(angleBase), (rayonBase + rayon1) * cos(angleBase) + rayon1 * cos(angle1), (rayonBase + rayon1) * sin(angleBase) + rayon1 * sin(angle1));
  vertices = (PVector[])append(vertices, new PVector((rayonBase + rayon1) * cos(angleBase) + rayon1 * cos(angle1), (rayonBase + rayon1) * sin(angleBase) + rayon1 * sin(angle1)));
  
  strokeWeight(1);
  beginShape();
  stroke(255, 0, 100);
  for(PVector vertice : vertices){
    vertex(vertice.x, vertice.y);
  }
  endShape();
  
  strokeWeight(8);
  stroke(255);
  point(0, 0);
  point((rayonBase + rayon1) * cos(angleBase), (rayonBase + rayon1) * sin(angleBase));
  point((rayonBase + rayon1) * cos(angleBase) + rayon1 * cos(angle1), (rayonBase + rayon1) * sin(angleBase) + rayon1 * sin(angle1));
  
  angleBase -= 0.1;
  angle1 -= 0.5;
  
}
