class Ball{
  
  float rayon, angle, vit;
  color c;
  PVector pos, dir;
  
  Ball(float x, float y){
    rayon = random(10, 30);
    pos = new PVector(x, y);
    angle = radians(random(360));
    dir = PVector.fromAngle(angle);
    vit = random(1, 3);
    dir.mult(vit);
    c = color(round(random(255)), round(random(255)), round(random(255)));
  }
  
  void update(){
    if(pos.add(dir).x - rayon < 0){
      angle = 3 * PI - angle;
    dir = PVector.fromAngle(angle);
    dir.mult(vit);
    }
    if(pos.add(dir).x + rayon > width){
      angle = 3 * PI - angle;
    dir = PVector.fromAngle(angle);
    dir.mult(vit);
    }
    if(pos.add(dir).y - rayon < 0){
      angle = 2 * PI - angle;
    dir = PVector.fromAngle(angle);
    dir.mult(vit);
    }
    if(pos.add(dir).y + rayon > height){
      angle = 2 * PI - angle;
    dir = PVector.fromAngle(angle);
    dir.mult(vit);
    }
    pos.add(dir);
  }
  
  void draw(){
    fill(c);
    noStroke();
    circle(pos.x, pos.y, rayon*2);
  }
}
