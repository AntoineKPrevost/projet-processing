Ball[] balls;

void setup(){
  fullScreen();
  //size(600, 600);
  balls = new Ball[50];
  for(int i = 0; i < 50; i++){
    balls[i] = new Ball(random(50, width - 50), random(50, height - 50));
  }
}

void draw(){
  background(0);
  for(int i = 0; i < balls.length; i++){
    if(balls[i] != null){
      balls[i].draw();
      balls[i].update();
    }
  }
}
