class Trieur{
// Concernant tout
  FloatList values;
  int index;
  int lindex;
  int tmplindex;
  int findex;
  int tmpfindex;
  int sens;
  int passage;
  
  void init(int nbVal, int maxVal){
    index = 0;
    lindex = nbVal + 1;
    tmplindex = 0;
    findex = -1;
    tmpfindex = 0;
    sens = 1;
    passage = 0;
    
    values = new FloatList();
    for (int i = 0; i < nbVal; i++) {
      values.set(i, i*maxVal/(float)nbVal);
    }
    values.shuffle();
  }
  
  FloatList getValues(){
    return values;
  }
  
  int getIndex(){
    return index;
  }
  
  Float getValue(int index){
    return values.get(index);
  }
  
  boolean bulle() {
    if (values.get(index) > values.get(index+1)) {
      float temp = values.get(index);
      values.set(index, values.get(index + 1));
      values.set(index + 1, temp);
    }
    index ++;
    if (index == values.size() - 1 - passage) {
      index = 0;
      passage++;
    }
    if (passage == values.size() - 1) {
      return true;
    }
    return false;
  }
  
  boolean shake() {
    if (sens == 1) {
      if (values.get(index) > values.get(index+1)) {
        float temp = values.get(index);
        values.set(index, values.get(index + 1));
        values.set(index + 1, temp);
        tmplindex = index + 1;
      }
  
      index++;
      if (index == values.size() - 1 - passage || index == lindex) {
        passage++;
        sens = -1;
        lindex = tmplindex;
      }
      if (passage == values.size()/2 - 1) {
        return true;
      }
    } else {
      if (values.get(index) < values.get(index-1)) {
        float temp = values.get(index);
        values.set(index, values.get(index - 1));
        values.set(index - 1, temp);
        tmpfindex = index - 1;
      }
  
      index--;
      if (index == passage-1 || index == findex) {
        sens = 1;
        findex = tmpfindex;
      }
      if (passage == values.size()/2 - 1) {
        return true;
      }
    }
    if (findex == lindex) {
      return true;
    }
    return false;
  }
}
