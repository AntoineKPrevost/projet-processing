class Utils{
  color calcCol(float value) {
    int r = 255;
    int g = 255;
    int b = 255;
    float ratio = 1530 / (float)height;
    if (value <= height / 6) {
      r = 255;
      g = round(value * ratio);
      b = 0;
    } else if (value <= 2 *height / 6) {
      r = 510 - round(value * ratio);
      g = 255;
      b = 0;
    } else if (value <= 3 *height / 6) {
      r = 0;
      g = 255;
      b = round(value * ratio) - 510 ;
    } else if (value <= 4 *height / 6) {
      r = 0;
      g = 1020 - round(value * ratio);
      b = 255;
    } else if (value <= 5 *height / 6) {
      r = round(value * ratio) - 1020;
      g = 0;
      b = 255;
    } else if (value <= height) {
      r = 255;
      g = 0;
      b = 1530 - round(value * ratio);
    }
    return color(r, g, b);
  }
}
