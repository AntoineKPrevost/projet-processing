class Menu{

  void draw(){
    textSize(32);
    textAlign(CENTER, CENTER);
    text("Lancer", (width / 2) - 55, (height / 2 ) - 20, 110, 40);
    fill(0, 102, 153);
    stroke(0, 102, 153);
    dessinCarre(width / 8, height / 2 + 60);
    text("Tri bulle", (width / 8) + 20, (height / 2 ) + 35, 150, 40);
    dessinCarre(width / 8, height / 2 + 120);
    text("Tri cocktail", (width / 8) + 20, (height / 2 ) + 95, 200, 40);
  }
  
  boolean posLancer(){
    return mouseX > (width / 2) - 55 && mouseY > (height / 2 ) - 20
      && mouseX < (width / 2) + 55 && mouseY < (height / 2 ) + 20;
  }
  
  boolean posBulle(){
    return mouseX > (width / 8) - 10 && mouseY > (height / 2 ) +50
      && mouseX < (width / 8) + 200 && mouseY < (height / 2 ) + 70;
  }
  
  boolean posCocktail(){
    return mouseX > (width / 8) - 10 && mouseY > (height / 2 ) +110
      && mouseX < (width / 8) + 200 && mouseY < (height / 2 ) + 130;
  }

  void dessinCarre(int axeX, int axeY){
    line(axeX - 10, axeY - 10, axeX + 10, axeY - 10);
    line(axeX + 10, axeY - 10, axeX + 10, axeY + 10);
    line(axeX + 10, axeY + 10, axeX - 10, axeY + 10);
    line(axeX - 10, axeY + 10, axeX - 10, axeY - 10);
  }
  
  void croixTri(int choix){
    line(width / 8 - 10, height / 2 + 60 * choix - 10, width / 8 + 10, height / 2 + 60 * choix + 10);
    line(width / 8 - 10, height / 2 + 60 * choix + 10, width / 8 + 10, height / 2 + 60 * choix - 10);
  }
}
