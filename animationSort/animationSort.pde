Utils outil = new Utils();
Trieur tri = new Trieur();
Menu menu = new Menu();

boolean finTri = false;

int choixTri;
final int BULLE = 1;
final int COCKTAIL = 2;
final int TAS = 3;
final int RAPIDE = 4;

int affichage;
final int BARRE = 1;
final int CERCLE = 2 ;

int option;
final int MONOCHROME = 1;
final int COULEUR = 2 ;

int etat;
final int MENU = 0;
final int INIT = 1;
final int TRIAGE = 2;

void setup() {
  //fullScreen();
  size(800, 500);
  etat = MENU;
  choixTri = BULLE;
  affichage = BARRE;
  option = COULEUR;
}

void draw() {
  background(0);
  
  switch(etat){
    case MENU :
      menu.draw();
      menu.croixTri(choixTri);
      if(menu.posLancer()){
        stroke(0, 102, 153);
        line(width / 2 - 55, height /2 + 25, width / 2 + 55, height /2 + 25);
        if(mousePressed){
          changeEtat(INIT);
        }
      }
      if(menu.posBulle() && mousePressed){
        choixTri = 1;
      }
      if(menu.posCocktail() && mousePressed){
        choixTri = 2;
      }
      break;
    case INIT :
      finTri = false;
      tri.init(width, height);
      changeEtat(TRIAGE);
      break ;
    case TRIAGE :
      if(!finTri){
        for (int n = 0; n < width; n++) {
          switch(choixTri){
            case BULLE:
              finTri = tri.bulle();
              break;
            case COCKTAIL:
              finTri = tri.shake();
              break;
          }
          if(finTri){
            break;
          }
        }
      }else{
        if(mousePressed){
          changeEtat(MENU);
        }
      }
    
      // Affichage des valeurs
      switch(affichage){
        case BARRE:
          affichageBarre();
          break;
        case CERCLE:
          affichageCercle();
          break;
      }
      break;
  }
}

void changeEtat(int newEtat){
  etat = newEtat;
}

void changeAffichage(int newAffichage){
  affichage = newAffichage;
}

void changeOption(int newOption){
  option = newOption;
}

// affichage du tri
void affichageBarre(){
  for (int i = 0; i < tri.getValues().size(); i++) {
    if (option == COULEUR){
      stroke(outil.calcCol(tri.getValue(i)));
    }
    if (i == tri.getIndex() && !finTri) {
      stroke(255);
    }
    line(i, height, i, height - tri.getValue(i));
  }
}

void affichageCercle(){
  translate(width / 2, height / 2);
  for (int i = 0; i < tri.getValues().size(); i++) {
    float angle = - (5 * PI / 2) - (2 * PI / width) * i ;
    int rayInt = 15;
    int rayExt = height / 2 - 15;
    stroke(outil.calcCol(tri.getValue(i)));
    if (i == tri.getIndex() && !finTri) {
      stroke(255);
    }
    line(rayInt * cos(angle), rayInt * sin(angle), rayExt * cos(angle), rayExt * sin(angle));
  }
}
