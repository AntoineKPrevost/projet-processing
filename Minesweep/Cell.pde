class Cell{
  
  int posX, posY, numb;
  boolean mine;
  boolean reveal = false;
  boolean marked = false;
  
  Cell(int i, int j){
    posX = i;
    posY = j;
  }
  
  void addMine(){
    mine = true;
  }
  
  boolean checkMine(){
    return mine;
  }
  
  boolean checkReveal(){
    return reveal;
  }
  
  boolean checkMark(){
    return marked;
  }
  
  void mark(){
    marked = !marked;
  }
  
  void reveal(){
    reveal = true;
    if(!mine){
      if(numb == 0){
        if(posX > 0 && posY > 0){
          if(!grid[posX-1][posY-1].checkReveal()){
            grid[posX-1][posY-1].reveal();
          }
        }
        if(posY > 0){
          if(!grid[posX][posY-1].checkReveal()){
            grid[posX][posY-1].reveal();
          }
        }
        if(posX < cols - 1 && posY > 0){
          if(!grid[posX+1][posY-1].checkReveal()){
            grid[posX+1][posY-1].reveal();
          }
        }
        if(posX > 0){
          if(!grid[posX-1][posY].checkReveal()){
            grid[posX-1][posY].reveal();
          }
        }
        if(posX < cols - 1){
          if(!grid[posX+1][posY].checkReveal()){
            grid[posX+1][posY].reveal();
          }
        }
        if(posX > 0 && posY < rows - 1){
          if(!grid[posX-1][posY+1].checkReveal()){
            grid[posX-1][posY+1].reveal();
          }
        }
        if(posY < rows - 1){
          if(!grid[posX][posY+1].checkReveal()){
            grid[posX][posY+1].reveal();
          }
        }
        if(posX < cols - 1 && posY < rows - 1){
          if(!grid[posX+1][posY+1].checkReveal()){
            grid[posX+1][posY+1].reveal();
          }
        }
    }
    }
  }
  
  void checkNumber(){
    numb = 0;
    if(posX > 0 && posY > 0){
        if(grid[posX-1][posY-1].checkMine()){
          numb++;
        }
      }
      if(posY > 0){
        if(grid[posX][posY-1].checkMine()){
          numb++;
        }
      }
      if(posX < cols - 1 && posY > 0){
        if(grid[posX+1][posY-1].checkMine()){
          numb++;
        }
      }
      if(posX > 0){
        if(grid[posX-1][posY].checkMine()){
          numb++;
        }
      }
      if(posX < cols - 1){
        if(grid[posX+1][posY].checkMine()){
          numb++;
        }
      }
      if(posX > 0 && posY < rows - 1){
        if(grid[posX-1][posY+1].checkMine()){
          numb++;
        }
      }
      if(posY < rows - 1){
        if(grid[posX][posY+1].checkMine()){
          numb++;
        }
      }
      if(posX < cols - 1 && posY < rows - 1){
        if(grid[posX+1][posY+1].checkMine()){
          numb++;
        }
      }
  }
  
  void draw(){
    int alpha = 255;
    int beta = 205;
    if(reveal){
      alpha = 205;
      beta = 255;
      stroke(alpha / 2);
      fill(alpha);
      quad(posX * scl, posY * scl, (posX + 0.1) * scl, (posY + 0.1) * scl, (posX + 0.1) * scl, (posY + 0.9) * scl, posX * scl, (posY + 1) * scl);
      quad(posX * scl, posY * scl, (posX + 0.1) * scl, (posY + 0.1) * scl, (posX + 0.9) * scl, (posY + 0.1) * scl, (posX+ 1) * scl, posY * scl);
      stroke(beta / 2);
      fill(beta);
      quad((posX + 1) * scl, (posY + 1) * scl, (posX + 0.9) * scl, (posY + 0.9) * scl, (posX + 0.1) * scl, (posY + 0.9) * scl, posX * scl, (posY + 1) * scl);
      quad((posX + 1) * scl, (posY + 1) * scl, (posX + 0.9) * scl, (posY + 0.9) * scl, (posX + 0.9) * scl, (posY + 0.1) * scl, (posX+ 1) * scl, posY * scl);
      rect((posX + 0.1) * scl, (posY + 0.1) * scl, 0.8 * scl, 0.8 * scl);
      noFill();
      if(mine){
        circle((posX + 0.5) * scl, (posY + 0.5) * scl, scl * 0.7);
      }else{
        if(numb !=0){
          textSize(scl * 0.8);
          fill(0);
          text(numb, (posX + 0.2) * scl, (posY + 0.8) * scl);
          noFill();
        }
      }
    }else{
      stroke(alpha / 2);
      fill(alpha);
      quad(posX * scl, posY * scl, (posX + 0.1) * scl, (posY + 0.1) * scl, (posX + 0.1) * scl, (posY + 0.9) * scl, posX * scl, (posY + 1) * scl);
      quad(posX * scl, posY * scl, (posX + 0.1) * scl, (posY + 0.1) * scl, (posX + 0.9) * scl, (posY + 0.1) * scl, (posX+ 1) * scl, posY * scl);
      stroke(beta / 2);
      fill(beta);
      quad((posX + 1) * scl, (posY + 1) * scl, (posX + 0.9) * scl, (posY + 0.9) * scl, (posX + 0.1) * scl, (posY + 0.9) * scl, posX * scl, (posY + 1) * scl);
      quad((posX + 1) * scl, (posY + 1) * scl, (posX + 0.9) * scl, (posY + 0.9) * scl, (posX + 0.9) * scl, (posY + 0.1) * scl, (posX+ 1) * scl, posY * scl);
      rect((posX + 0.1) * scl, (posY + 0.1) * scl, 0.8 * scl, 0.8 * scl);
      noFill();
      if(marked){
        textSize(scl * 0.8);
        fill(0);
        text("?", (posX + 0.3) * scl, (posY + 0.8) * scl);
        noFill();
      }
    }
  }
}
