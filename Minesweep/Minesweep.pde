int cols, rows;
int scl = 80;

int mineMax = 15;

Cell[][] grid;

void setup(){
  size(800, 800);
  cols = width / scl;
  rows = height / scl;
  grid = new Cell[cols][rows];
  initial();
}

void draw(){
  background(255);
  
  stroke(0);
  noFill();
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j].draw();
      stroke(0);
      rect(i * scl, j * scl, scl, scl);
    }
  }
}

void initial(){
  for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++){
      grid[i][j] = new Cell(i, j);
    }
  }
  int nbMine = 0;
  while(nbMine < mineMax){
    int x = floor(random(cols));
    int y = floor(random(rows));
    if(!grid[x][y].checkMine()){
      grid[x][y].addMine();
      nbMine ++;
    }
  }
  for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++){
      grid[i][j].checkNumber();
    }
  }
}

void keyPressed(){
  if(key == 'r'){
    initial();
  }
}

void mousePressed(){
  if(!grid[floor(mouseX/scl)][floor(mouseY/scl)].checkReveal()){
    if (mouseButton == LEFT) {
        grid[floor(mouseX/scl)][floor(mouseY/scl)].reveal();
    }else if(mouseButton == RIGHT){
        grid[floor(mouseX/scl)][floor(mouseY/scl)].mark();
    }
  }
}
