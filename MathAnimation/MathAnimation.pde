int count;
int start;

void setup(){
  size(800,800);
  background(0);
  count = 0;
  start = 0;
}

void draw(){
  if(start == 1){
    count++; 
    nextPoint(count, count * 137.5);
  }else{
    if(mousePressed){
      start++;
    }
  }
}

void nextPoint(float rayon, float angle){
  translate(width/2, height/2);
  stroke(255);
  ellipse(rayon * cos(angle), rayon * sin(angle), 5, 5);
}
