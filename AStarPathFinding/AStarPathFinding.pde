int scl = 25;
int cols, rows;
color colorPath;

Cell[][] grid;
ArrayList<Cell> openList = new ArrayList();
ArrayList<Cell> closeList = new ArrayList();
Cell depart, objectif;

void setup() {
  //size(600, 600);
  fullScreen();
  cols = width / scl;
  rows = height / scl;
  grid = new Cell[cols][rows];
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      grid[i][j] = new Cell(i, j);
    }
  }
  depart = grid[0][rows-1];
  depart.fScore = 0;
  depart.gScore = 0;
  depart.hScore = 0;
  depart.acces = true;
  objectif = grid[cols-1][0];
  objectif.acces = true;
  openList.add(depart);
}

void draw() {
  frameRate(2);

  //Affichage
  background(255);
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      grid[i][j].draw(color(255));
    }
  }

  for (Cell c : openList) {
    c.draw(color(0, 255, 0));
  }

  for (Cell c : closeList) {
    c.draw(color(255, 0, 0));
  }

  //Pathfinding
  if (openList.size() > 0) {
    java.util.Collections.sort(openList);
    Cell current = openList.remove(0);
    current.draw(color(255, 0, 255));
    objectif.draw(color(255,0, 255));
    closeList.add(current);
    colorPath = color(255, 0, 255);
    if (current == objectif) {
      noLoop();
      colorPath = color(0, 0, 255);
      print("fini");
    }
    float tempCout = 0;
    ArrayList<Cell> neighbors = current.getNeighbors();
    for (Cell c : neighbors) {
        tempCout = current.gScore + distance(c, current);
      if (closeList.indexOf(c) == -1 && c.fScore < tempCout) {
        if (openList.indexOf(c) == -1) {
          openList.add(c);
        }
        if (c.gScore < tempCout) {
            c.origine = current;
            c.hScore = distance(c, objectif);
            c.fScore = c.gScore + c.hScore;
          }
      }
      
    ArrayList<Cell> path = getPath(current);
    for (Cell cell : path) {
      stroke(colorPath);
      strokeWeight((scl - 4) / 2);
      line((cell.posX + 0.5) * scl, (cell.posY + 0.5) * scl, (cell.origine.posX + 0.5) * scl, (cell.origine.posY + 0.5) * scl);
    }
    }
  }
}


ArrayList<Cell> getPath(Cell c) {
  ArrayList<Cell> path = new ArrayList();
  while (c.origine != null) {
    path.add(c);
    c = c.origine;
  }
  return path;
}

float distance(Cell from, Cell goal) {
  return dist(from.posX, from.posY, goal.posX, goal.posY);
}
