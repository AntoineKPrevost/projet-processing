class Cell  implements Comparable {
  int posX, posY;
  float fScore = -1;
  float gScore = -1;
  float hScore = -1;
  boolean acces = true;
  Cell origine;

  Cell(int x, int y) {
    posX = x;
    posY = y;
    acces = random(1) > 0.35;
  }

  ArrayList<Cell> getNeighbors() {
    ArrayList neighbors = new ArrayList();
    if (posX > 0) {
      if (grid[posX - 1][posY].acces) {
        neighbors.add(grid[posX - 1][posY]);
      }
    }
    if (posX < cols - 1) {
      if (grid[posX + 1][posY].acces) {
        neighbors.add(grid[posX + 1][posY]);
      }
    }
    if (posY > 0) {
      if (grid[posX][posY - 1].acces) {
        neighbors.add(grid[posX][posY - 1]);
      }
    }
    if (posY < rows - 1) {
      if (grid[posX][posY + 1].acces) {
        neighbors.add(grid[posX][posY + 1]);
      }
    }

    if (posX > 0 && posY > 0) {
      if (grid[posX - 1][posY - 1].acces) {
        neighbors.add(grid[posX - 1][posY - 1]);
      }
    }
    if (posX < cols - 1 && posY > 0) {
      if (grid[posX + 1][posY - 1].acces) {
        neighbors.add(grid[posX + 1][posY - 1]);
      }
    }
    if (posX > 0 && posY > rows - 1) {
      if (grid[posX - 1][posY + 1].acces) {
        neighbors.add(grid[posX - 1][posY + 1]);
      }
    }
    if (posX < cols - 1 && posY < rows - 1) {
      if (grid[posX + 1][posY + 1].acces) {
        neighbors.add(grid[posX + 1][posY + 1]);
      }
    }
    return neighbors;
  }

  void draw(color c) {
    noStroke();
    if (acces) {
      fill(c);
    } else {
      fill(0);
    }
    circle((posX + 0.5) * scl, (posY + 0.5) * scl, scl / 2);
  }

  int compareTo(Object o) {
    Cell autre = (Cell)o;
    if (fScore > autre.fScore) {
      return 1;
    }
    if (fScore < autre.fScore) {
      return -1;
    }
    return 0;
  }
}
