float angle;

void setup(){
  size(600, 600);
  angle = 30;
}

void draw(){
  background(0);
  
  pushMatrix();
  translate(width / 2, height);
  branch(150);
  popMatrix();
}

void branch(float lon){
  stroke(255);
  line(0, 0, 0, - lon);
  if(lon > 2){
    pushMatrix();
    translate(0, - lon);
    rotate(radians(angle));
    branch(lon * 0.69);
    popMatrix();
    pushMatrix();
    translate(0, - lon);
    rotate( - radians(angle));
    branch(lon * 0.69);
    popMatrix();
  }
}
