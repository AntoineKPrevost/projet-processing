int s, m, h;
float diam, diamInt;

void setup(){
  fullScreen();
  //size(600, 600);
  diam = width == height ? width * 0.8 : (width > height ? height * 0.8 : width * 0.8);
  diamInt = diam * 0.5;
}

void draw(){
  
  background(0);
  
  s = second();
  m = minute();
  h = hour();
  
  translate(width / 2, height / 2);
  
  noFill();
  stroke(255);
  strokeWeight(8);
  //circle(0, 0, diam);
  //circle(0, 0, diam * 0.9);
  //circle(0, 0, diam * 0.8);

  rotate(- PI / 2);
  stroke(100, 150, 255);
  arc(0, 0, diam * 0.8, diam * 0.8, radians(0), (2 * PI * (h % 12))/ 12);
  line(0, 0, diamInt * 0.4 / 2 * cos((2 * PI * (h % 12))/ 12), diamInt * 0.4 / 2 * sin((2 * PI * (h % 12))/ 12));
  stroke(150, 255, 100);
  arc(0, 0, diam * 0.9, diam * 0.9, radians(0), (2 * PI * m)/ 60);
  line(0, 0, diamInt * 0.7 / 2 * cos((2 * PI * m)/ 60), diamInt * 0.7 / 2 * sin((2 * PI * m)/ 60));
  stroke(255, 100, 150);
  arc(0, 0, diam, diam, radians(0), (2 * PI * s)/ 60);
  strokeWeight(4);
  line(0, 0, diamInt / 2 * cos((2 * PI * s)/ 60), diamInt / 2 * sin((2 * PI * s)/ 60));
  strokeWeight(12);
  stroke(255, 100, 150);
  point(diam / 2 * cos((2 * PI * s)/ 60), diam / 2 * sin((2 * PI * s)/ 60));
  stroke(150, 255, 100);
  point(diam * 0.9 / 2 * cos((2 * PI * m)/ 60), diam * 0.9 / 2 * sin((2 * PI * m)/ 60));
  stroke(100, 150, 255);
  point(diam * 0.8 / 2 * cos((2 * PI * (h % 12))/ 12), diam * 0.8 / 2 * sin((2 * PI * (h % 12))/ 12));
}
