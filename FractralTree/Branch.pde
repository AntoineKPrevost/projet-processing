class Branch{
  
  PVector deb, fin;
  float angle;
  boolean grooth;
  
  Branch(PVector d, PVector f, float a){
    deb = d;
    fin = f;
    angle = a;
  }
  
  void draw(){
    stroke(255);
    line(deb.x, deb.y, fin.x, fin.y);
  }
}
