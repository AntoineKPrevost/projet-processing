Branch[] branches = {};
float lon, angle;

void setup() {
  fullScreen();
  //size(600, 600);
  lon = 200;
  angle = 30;
  branches = (Branch[])append(branches, new Branch(new PVector(width / 2, height), new PVector(width / 2, height - lon), 0));
}

void draw() {
  frameRate(5);
  background(0);
  for (Branch branche : branches) {
    branche.draw();
  }
  if (lon > 4) {
    lon *= 0.69;
    for (Branch branche : branches) {
      if (!branche.grooth) {
        PVector pDeb = new PVector(branche.fin.x, branche.fin.y);
        PVector pFin1 = new PVector(branche.fin.x + lon * cos(radians(branche.angle + angle) - PI / 2), branche.fin.y + lon * sin(radians(branche.angle + angle) - PI / 2));
        PVector pFin2 = new PVector(branche.fin.x + lon * cos(radians(branche.angle - angle) - PI / 2), branche.fin.y + lon * sin(radians(branche.angle - angle) - PI / 2));
        branches = (Branch[])append(branches, new Branch(pDeb, pFin1, branche.angle + angle));
        branches = (Branch[])append(branches, new Branch(pDeb, pFin2, branche.angle - angle));
        branche.grooth = true;
      }
    }
  }
}
