int cols, rows;
int scl = 10;

boolean started = false;

int[][] grid, next;

void setup(){
  //fullScreen();
  size(800, 800);
  cols = width / scl;
  rows = height / scl;
  grid = new int[cols][rows];
  next = new int[cols][rows];
  initGrid();
}

void draw(){
  frameRate(15);
  background(255);
  noFill();
  stroke(0, 50);
  if(started){
    for(int i = 0; i < cols; i++){
      for(int j = 0; j < rows; j++){
        next[i][j] = evolution(i,j);
      }
    }
    grid = next;
    next = new int[cols][rows];
  }
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      noFill();
      if(grid[i][j] == 1){
        fill(0);
      }
      rect(scl * i, scl * j, scl, scl);
    }
  }
}

int evolution(int i, int j){
  int val = 0;
  if(i > 0 && j > 0){
    if(grid[i - 1][j - 1] == 1){
      val++;
    }
  }
  if(j > 0){
    if(grid[i][j - 1] == 1){
      val++;
    }
  }
  if(i < cols -1 && j > 0){
    if(grid[i + 1][j - 1] == 1){
      val++;
    }
  }
  if(i > 0){
    if(grid[i - 1][j] == 1){
      val++;
    }
  }
  if(i < cols - 1){
    if(grid[i + 1][j] == 1){
      val++;
    }
  }
  if(i > 0 && j < rows - 1){
    if(grid[i - 1][j + 1] == 1){
      val++;
    }
  }
  if(j < rows - 1){
    if(grid[i][j + 1] == 1){
      val++;
    }
  }
  if(i < cols - 1 && j < rows - 1){
    if(grid[i + 1][j + 1] == 1){
      val++;
    }
  }
  
  if(grid[i][j] == 1){
    if(val == 2 || val == 3){
      return 1;
    }else{
      return 0;
    }
  }else{
    if(val == 3){
      return 1;
    }else{
      return 0;
    }
  }
}

void initGrid(){
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j] = random(100)> 50 ? 1 : 0;
    }
  }
}

void keyPressed(){
  if(key == 'e'){
    for(int i = 0; i < cols; i++){
      for(int j = 0; j < rows; j++){
        grid[i][j] = 0;
      }
    }
    started = false;
  }
  if(key == 'r'){
    initGrid();
    started = false;
  }
  if(key == ' '){
    started = true;
  }
}

void mousePressed(){
  if(!started){
    int col = floor(mouseX / scl);
    int row = floor(mouseY / scl);
    if(grid[col][row] == 1){
      grid[col][row] = 0;
    }else{
      grid[col][row] = 1;
    }
  }
}
