int mod = 993;
int ray = 250;
float table = 399;
/*
  valeurs intéressante :
    table | mod 
      41  | 84 
      55  | 84 
     399  | 993 
*/


void setup(){
  fullScreen();
  //size(600, 600);
  ray = width > height ? (height - 100) / 2 : (width - 50) / 2;
}

void draw(){
  //frameRate(10);
  background(0);
  text("table de " + table + " modulo " + mod, 10, 10);
  
  translate(width / 2, height / 2);
  rotate( - PI/2);
  strokeWeight(1);
  stroke(255);
  noFill();
  circle(0, 0, ray * 2);
  for(int i = 0; i < mod; i++){
    strokeWeight(5);
    point(ray * cos((2 * i * PI) / mod), ray * sin((2 * i * PI) / mod));
  }
  for(int i = 0; i < mod; i++){
    strokeWeight(1);
    line(ray * cos((2 * i * PI) / mod), ray * sin((2 * i * PI) / mod), ray * cos(((table * i) % mod * 2 * PI) / mod), ray * sin(((table * i) % mod * 2 * PI) / mod));
  }
}
