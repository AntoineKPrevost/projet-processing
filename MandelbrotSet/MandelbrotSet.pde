float bornMinX = -2.5;
float bornMaxX = 2;
float bornMinY = -2.5;
float bornMaxY = 2;
int iter = 500;

//bool

void setup() {
  size(800, 800);
}

void draw() {

  background(0);
  color c = color(0);

  loadPixels();

  for ( int y = 0; y < height; y++) {
    for ( int x = 0; x < width; x ++) {
      double a = map(x, 0, width, bornMinX, bornMaxX);
      double b = map(y, 0, height, bornMinY, bornMaxY);
      double ao = a;
      double bo = b;

      int nb = 0;
      while (nb < iter) {
        double an = a*a - b*b + ao;
        double bn = bo + 2*a*b; 

        a = an;
        b = bn;
        if (Math.abs(an + bn) > 16) {
          break;
        }
        nb++;
      }
      c = color(map(nb, iter, 0, 0, 255));
      pixels[y*height + x] = c;
    }
  }

  updatePixels();
  
  stroke(255, 0, 0);
  line(0, width / 2, height, width / 2);
  line(height / 2, 0, height / 2, width);
  stroke(0);
}

void mouseDragged() {
  bornMinX += map(pmouseX, 0, width, bornMinX, bornMaxX) - map(mouseX, 0, width, bornMinX, bornMaxX);
  bornMaxX += map(pmouseX, 0, width, bornMinX, bornMaxX) - map(mouseX, 0, width, bornMinX, bornMaxX);
  bornMinY += map(pmouseY, 0, height, bornMinY, bornMaxY) - map(mouseY, 0, height, bornMinY, bornMaxY);
  bornMaxY += map(pmouseY, 0, height, bornMinY, bornMaxY) - map(mouseY, 0, height, bornMinY, bornMaxY);
}

void mouseWheel(MouseEvent event) {
  float e = event.getCount() < 0 ? 1 : -1;
  float borneX = (bornMaxX - bornMinX) * 0.1;
  float borneY = (bornMaxY - bornMinY) * 0.1;
  
  bornMinX += e * borneX;
  bornMaxX -= e * borneX;
  bornMinY += e * borneY;
  bornMaxY -= e * borneY;
}
