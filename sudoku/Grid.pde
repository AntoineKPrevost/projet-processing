class Grid{
  
  Cell[][] grille = new Cell[9][9];
  int cellXFocus = -1;
  int cellYFocus = -1;
  
  public Grid(){
      for(int i = 0; i < 9; i++){
        for(int j = 0; j < 9; j++){
          grille[i][j] = new Cell(i, j);
        }
      }
  }
  
  public Grid(int[][] base){
    for(int i = 0; i < 9; i++){
      for(int j = 0; j < 9; j++){
        grille[i][j] = new Cell(i, j, base[i][j]);
      }
    }
  }
  
  public void draw(){
    strokeWeight(2);
    line(0, 0, 0, 450);
    line(150, 0, 150, 450);
    line(300, 0, 300, 450);
    line(450, 0, 450, 450);
    
    line(0, 0, 450, 0);
    line(0, 150, 450, 150);
    line(0, 300, 450, 300);
    line(0, 450, 450, 450);
    
    if(cellXFocus != -1 && cellYFocus != -1){
      translate(cellXFocus * 50, cellYFocus * 50);
      line(5, 5, 5, 17);
      line(5, 33, 5, 45);
      line(5, 5, 17, 5);
      line(5, 45, 17, 45);
      line(33, 5, 45, 5);
      line(45, 5, 45, 17);
      line(33, 45, 45, 45);
      line(45, 33, 45, 45);
      translate(- cellXFocus * 50, - cellYFocus * 50);
    }
    
    strokeWeight(1);
    line(50, 0, 50, 450);
    line(100, 0, 100, 450);
    line(200, 0, 200, 450);
    line(250, 0, 250, 450);
    line(350, 0, 350, 450);
    line(400, 0, 400, 450);
    line(0, 50, 450, 50);
    line(0, 100, 450, 100);
    line(0, 200, 450, 200);
    line(0, 250, 450, 250);
    line(0, 350, 450, 350);
    line(0, 400, 450, 400);

    for(Cell[] cells : grille){
      for(Cell cell : cells){
        cell.draw();
      }  
    }
  }
  
  public void setFocus(float mX, float mY){
    cellXFocus = floor((mX - 10) / 50);
    cellYFocus = floor((mY - 10) / 50);
  }
  
  public void setUnfocus(){
    cellXFocus = -1;
    cellYFocus = -1;
  }
  
  public void setSelected(){
    for(int j = 0; j < 9; j++){
      for(int i = 0; i < 9; i++){
        if(cellXFocus == j && cellYFocus == i){
          grille[i][j].selected = true;
        }else{
          grille[i][j].selected = false;
        }
      }
    }
  }
  
  public void setValue(int val){
    for(int j = 0; j < 9; j++){
      for(int i = 0; i < 9; i++){
        if(grille[i][j].selected && !grille[i][j].initial){
          grille[i][j].nb = val;
        }
      }  
    }
  }
  
  public Cell getCell(int col, int row){
    return grille[row][col];
  }
  
  public void verification(){
    for(Cell[] cells : grille){
      for(Cell cell : cells){
        cell.valide = verificationCell(cell.col, cell.row, cell.nb);
      }  
    }
  }
  
  public boolean verificationCell(int col, int row, int value){
    boolean faute = false;
    for(int i = 0; i < 9; i ++){
      if(grille[i][col].nb == value && i != row){
         faute = true;
      }
    }
    for(int i = 0; i < 9; i ++){
      if(grille[row][i].nb == value && i != col){
         faute = true;
      }
    }
    
    int iRow = 0;
    int iCol = 0;
    
    switch(row){
      case 0:
      case 1:
      case 2:
        iRow = 0;
        break;
      case 3:
      case 4:
      case 5:
        iRow = 3;
        break;
      case 6:
      case 7:
      case 8:
        iRow = 6;
        break;
    }
    switch(col){
      case 0:
      case 1:
      case 2:
        iCol = 0;
        break;
      case 3:
      case 4:
      case 5:
        iCol = 3;
        break;
      case 6:
      case 7:
      case 8:
        iCol = 6;
        break;
    }
    for(int i = iRow; i < iRow + 3; i++){
      for(int j = iCol; j < iCol + 3; j++){
        if(grille[i][j].nb == value && i != row && j != col){
           faute = true;
        }
      }
    }
    
    return !faute;
    
  }
  
}
