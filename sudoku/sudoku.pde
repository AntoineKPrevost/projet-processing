import java.util.List;
import java.util.Arrays;

boolean lsolver = false;

Grid grille;
Solver solv;
Character[] lstChar = new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
List<Character> lst = Arrays.asList(lstChar); 
int[][] baseGrille = new int[][]{
  {0, 0, 0, 4, 0, 0, 8, 7, 0},
  {0, 4, 7, 0, 9, 2, 0, 5, 0},
  {2, 0, 0, 6, 0, 0, 0, 3, 0},
  {9, 7, 0, 5, 0, 0, 2, 0, 3},
  {5, 0, 8, 0, 2, 4, 7, 0, 6},
  {6, 0, 4, 0, 0, 7, 0, 8, 5},
  {0, 9, 0, 3, 0, 8, 0, 0, 7},
  {0, 0, 3, 2, 4, 0, 1, 6, 0},
  {0, 1, 2, 0, 0, 0, 0, 9, 0}
  };

void setup(){
  size(470, 470); 
  grille = new Grid(baseGrille);
  solv = new Solver(grille);
}

void draw(){
  background(255);
  
  translate(10, 10);
  grille.verification();
  grille.draw();
  
  if(lsolver){
    solv.step();
  }
  
}

void mouseMoved(){
  if(mouseX > 10 && mouseY > 10 && mouseX < 460 && mouseY < 460){
    grille.setFocus(mouseX, mouseY);
  }else{
    grille.setUnfocus();
  }
}

void mousePressed(){
  grille.setSelected();
}

void keyPressed(){
  if(lst.contains(key)){
    grille.setValue(Integer.parseInt(String.valueOf(key)));
  }
  
  if(key == ' '){
    lsolver = !lsolver;
  }
}
