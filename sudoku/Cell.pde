class Cell{
  public int nb;
  public int row;
  public int col;
  public boolean initial = false; 
  public boolean selected = false;
  public boolean valide = true;
  public boolean[] indice = {
  false, 
  false, 
  false, 
  false, 
  false, 
  false, 
  false, 
  false, 
  false};
  
  public Cell(int i, int j){
    nb = 0;
    col = j;
    row = i;
  }
  
  public Cell(int i, int j, int val){
    nb = val;
    initial = val != 0;
    col = j;
    row = i;
  }
  
  public void draw(){
    textSize(32);
    if(nb != 0){
      if(valide){
        if(initial){
          fill(0); 
        }else{
          fill(0, 200);
        }
      }else{
        if(initial){
          fill(255, 0, 0); 
        }else{
          fill(255, 0, 0, 200);
        }
      }
      text(nb, 15 + 50 * col, 36 + 50 * row);
    }
    
    if(selected){
      stroke(255, 0, 0);
      strokeWeight(2);
      translate(col * 50, row * 50);
      line(5, 5, 5, 17);
      line(5, 33, 5, 45);
      line(5, 5, 17, 5);
      line(5, 45, 17, 45);
      line(33, 5, 45, 5);
      line(45, 5, 45, 17);
      line(33, 45, 45, 45);
      line(45, 33, 45, 45);
      translate(- col * 50, - row * 50);
      stroke(0);
      strokeWeight(1);
    }
    
  }
   
}
