import java.util.Collections;

static class Solver{
  enum state {COMPTE, SELECT, VERIF};
  state etat;
  
  int timer;
  int index;
  int value;

  Cell current;
  
  Grid grille;
  List<Cell> lstUn;
  List<Cell> lstDeux;
  List<Cell> lstTrois;
  List<Cell> lstQuatre;
  List<Cell> lstCinq;
  List<Cell> lstSix;
  List<Cell> lstSept;
  List<Cell> lstHuit;
  List<Cell> lstNeuf;
  List<Cell> lst;
  
  public Solver(Grid grid){
    grille = grid;
    etat = state.COMPTE;
    index = 0;
    value = 1;
    lstUn = new ArrayList<Cell>();
    lstDeux = new ArrayList<Cell>();
    lstTrois = new ArrayList<Cell>();
    lstQuatre = new ArrayList<Cell>();
    lstCinq = new ArrayList<Cell>();
    lstSix = new ArrayList<Cell>();
    lstSept = new ArrayList<Cell>();
    lstHuit = new ArrayList<Cell>();
    lstNeuf = new ArrayList<Cell>();
    lst = new ArrayList<Cell>();
  }
  
  public void step(){
    timer --;
    if(timer <= 0){
      switch(etat){
        case COMPTE:
          compte();
          etat = state.SELECT;
          break;
        case SELECT:
          if(current != null){
            current.selected = false;
          }
          current = lst.get(index);
          current.selected = true;
          
          etat = state.VERIF;
          
          break;
        case VERIF:
          
          boolean res = false;
          
          for(int ver = value; ver <= 9; ver ++){
            res = grille.verificationCell(current.col, current.row, ver);
            if(res){
              current.nb = ver;
              break;
            }
          }
          
          if(res){
            if(index < lst.size() - 1){
              index ++;
              value = 1;
              etat = state.SELECT;
            }
          }else{
            index --;
            if(lst.get(index).nb !=9){
            value = lst.get(index).nb +1;
            lst.get(index).nb = 0;
            }
            etat = state.SELECT;
          }
          
          
          
          break;
      }
      timer = 10;
    }
  }
  
  public void compte(){
    for(int row = 0; row < 9; row++){
      for(int col = 0; col < 9; col++){
        int res= 0;
        
        for(int val = 1; val < 10; val ++){
          if(grille.getCell(col, row).nb == 0 && grille.verificationCell(col, row, val)){
            res ++;
          }
        }
        
        switch(res){
          case 1:
            lstUn.add(grille.getCell(col, row));
            break;
          case 2:
            lstDeux.add(grille.getCell(col, row));
            break;
          case 3:
            lstTrois.add(grille.getCell(col, row));
            break;
          case 4:
            lstQuatre.add(grille.getCell(col, row));
            break;
          case 5:
            lstCinq.add(grille.getCell(col, row));
            break;
          case 6:
            lstSix.add(grille.getCell(col, row));
            break;
          case 7:
            lstSept.add(grille.getCell(col, row));
            break;
          case 8:
            lstHuit.add(grille.getCell(col, row));
            break;
          case 9:
            lstNeuf.add(grille.getCell(col, row));
            break;
        }
      }
    }
    
    Collections.shuffle(lstUn);
    lst.addAll(lstUn);
    Collections.shuffle(lstDeux);
    lst.addAll(lstDeux);
    Collections.shuffle(lstTrois);
    lst.addAll(lstTrois);
    Collections.shuffle(lstQuatre);
    lst.addAll(lstQuatre);
    Collections.shuffle(lstCinq);
    lst.addAll(lstCinq);
    Collections.shuffle(lstSix);
    lst.addAll(lstSix);
    Collections.shuffle(lstSept);
    lst.addAll(lstSept);
    Collections.shuffle(lstHuit);
    lst.addAll(lstHuit);
    Collections.shuffle(lstNeuf);
    lst.addAll(lstNeuf);
    
  }
  
}
