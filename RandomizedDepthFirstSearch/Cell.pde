class Cell{
  
  int x, y;
  boolean[] walls = {true, true, true, true};
  boolean visited;
  
  Cell(int i, int j){
    x = i;
    y = j;
    visited = false;
  }

  void setVisited(){
    visited = true;
  }
  
  boolean getVisited(){
    return visited;
  }
  
  void remove(int index){
    walls[index] = false;
  }
  
  void draw(){
    if(visited){
      noStroke();
      fill(100, 0, 255, 75);
      rect(x*w, y*w, w, w);
    }
    stroke(0);
    noFill();
    if(walls[0]){
      line(x * w, y * w, (x + 1) * w, y * w);
    }
    if(walls[1]){
      line((x + 1) * w, y * w, (x + 1) * w, (y + 1) * w);
    }
    if(walls[2]){
      line((x + 1) * w, (y + 1) * w, x * w, (y + 1) * w);
    }
    if(walls[3]){
      line(x * w, (y + 1) * w, x * w, y * w);
    }
  }
}
