int cols, rows;
int w = 100;

Cell[][] grid;
Cell[] stack = {};
Cell current, next;

void setup(){
  fullScreen();
  //size(601, 601);
  cols = width / w;
  rows = height / w;
  grid = new Cell[cols][rows];
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j] = new Cell(i, j);
    }
  }
  current = grid[int(random(cols))][int(random(rows))];
  stack = (Cell[])append(stack, current);
  current.setVisited();
}

void draw(){
  frameRate(10);
  background(255);
  for(int i = 0; i < cols; i++){
    for(int j = 0; j < rows; j++){
      grid[i][j].draw();
      if(grid[i][j] == current){
        noStroke();
        fill(255, 0, 100, 100);
        rect(i*w, j*w, w, w);
      }
    }
  }
  if(stack.length != 0){
    Cell[] voisins = getVoisin(current);
    if(voisins.length != 0){
      int ind = int(random(voisins.length));
      next = voisins[ind];
      removeWall(current, next);
      stack = (Cell[])append(stack, next);
      grid[next.x][next.y].setVisited();
      current = next;
    }else{
      current = grid[stack[stack.length -1].x][stack[stack.length -1].y];
      stack = (Cell[])shorten(stack);
    }
  }
}

Cell[] getVoisin(Cell current){
  Cell[] voisin = {};
  if(current.x > 0 && !grid[current.x - 1][current.y].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x - 1][current.y]);
  }
  if(current.x < cols - 1 && !grid[current.x + 1][current.y].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x + 1][current.y]);
  }
  if(current.y > 0 && !grid[current.x][current.y - 1].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x][current.y - 1]);
  }
  if(current.y < rows - 1 && !grid[current.x][current.y + 1].getVisited()){
    voisin = (Cell[])append(voisin, grid[current.x][current.y + 1]);
  }
  return voisin;
}

void removeWall(Cell current, Cell next){
  switch(current.x - next.x){
    case -1 :
      grid[current.x][current.y].remove(1);
      grid[next.x][next.y].remove(3);
      break;
    case 1 :
      grid[current.x][current.y].remove(3);
      grid[next.x][next.y].remove(1);
      break;
  }
  switch(current.y - next.y){
    case -1 :
      grid[current.x][current.y].remove(2);
      grid[next.x][next.y].remove(0);
      break;
    case 1 :
      grid[current.x][current.y].remove(0);
      grid[next.x][next.y].remove(2);
      break;
  }
}
